VIPRELiveTools is a collection of utilities to improve the functionality of the VIPRE Live Antivirus Package by GFI Software.

VIPRE Live Antivirus official website: http://www.vipreantivirus.com/live/

These utilities are not officially associated with GFI Software in any way. If something goes wrong, it's not our fault.

List of utilities:

VIPRELiveDownloader: Using Wget (provided by GnuWin32), auto-downloads the VIPRE Live Antivirus Self-Extracting EXE.

VIPRERescueTempCleanup: Deletes the leftover files once the scan has finished; the leftover files can be fairly large (200-300MB), this script will quickly and easily delete the files.